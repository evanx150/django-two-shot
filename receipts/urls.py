from django.urls import path, include

from receipts.views import (
    ReceiptListViews,
)

urlpatterns = [
    path("", ReceiptListViews.as_view(), name="home"),
]
